import { useState } from 'react';

import commentsData from './data/comments.json'

import ArtworkComment from './components/ArtworkComment';
import NotificationButton from './components/NotificationButton';

function App() {
  
  const comments = commentsData
  const initialUnseenCommentsCount = comments.filter(comment => comment.acknowledged === false).length
  const [unseenCommentCount, setUnseenCommentCount] = useState(initialUnseenCommentsCount)

  const handleMarkAsRead = () =>{
    setUnseenCommentCount(unseenCommentCount-1)
  }
  return (
    <>
      <div className="top-menu-bar">
        <NotificationButton unseenCommentCount={unseenCommentCount}/>
      </div><br /><br />
      <ArtworkComment comments={comments} handleMarkAsRead={handleMarkAsRead}/>
    </>
    
  );
}

export default App;
