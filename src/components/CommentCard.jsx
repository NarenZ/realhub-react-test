import { useState } from "react";
import TimeAgo from "react-timeago";

const CommentCard = ({comment,handleMarkAsRead}) => {

    const [commentDate,commentTime] = comment.dates.created.date_time.split(" ")
    const [day,month,year] = commentDate.split("/");
    const comment_date = `${commentTime },${month},${day},${year}`

    const [commentIsAcknowledge,setCommentIsAcknowledge] = useState(comment.acknowledged)
    
    const handleOnAcknowledged = () => {
        setCommentIsAcknowledge(true)
        handleMarkAsRead()
    }

    return (
        <div>
            <div className="comment-card">
                <div className="user-image">
                    <img src={comment.user.image.original_url} alt="User" height={40} />
                </div>
                <div className="user-comment">
                    <div className="user-name">
                        {comment.user.full_name}
                    </div>
                    <div className="comment-body">
                        {comment.body}
                    </div>
                    <div className="comment-date">
                        <TimeAgo date={comment_date} /> {!commentIsAcknowledge && <span className="acknowledged-text" onClick={handleOnAcknowledged}>| Mark as Seen</span>}
                    </div>
                </div>
            </div>
            <hr className="dashed-hr" />
        </div> 
        
     );
}
 
export default CommentCard;