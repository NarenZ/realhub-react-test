import CommentCard from "./CommentCard";
import '../styles/comment.scss'
const ArtworkComment = ({comments,handleMarkAsRead}) => {

    return ( 
        <div className="comments-container">
            {
                comments.sort((a,b) => Date.parse(a.dates.created.date_time) > Date.parse(b.dates.created.date_time) ? 1 : -1).map((comment)=>{
                    return <CommentCard comment={comment} key={comment.id} handleMarkAsRead={handleMarkAsRead} />
                })
            }
        </div>
     );
}
 
export default ArtworkComment;